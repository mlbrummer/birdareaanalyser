/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.nomi.bird_area_analyser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author michiel
 */
public class BirdAreaCollection {
    private final HashMap<String, Area> collection = new HashMap<>();

    /**
     *
     * @param area the area
     */
    public void addArea(Area area) {
        this.collection.put(area.getName(), area);
    }

    /**
     * prints sorted using the given Comparator.
     * @param comp the comparator
     */
    public void printSorted(Comparator comp) {
        List<Area> values = new ArrayList<>();
        values.addAll(collection.values());
        Collections.sort(values, comp);
        values.stream().forEach((a) -> {
            System.out.println(a);
        });
    }

    /**
     * prints the default way: by name.
     */
    public void printSorted() {
        printSorted(new NameComparator());
    }

    /**
     * class to be used for sorting areas on name.
     */
    public static class NameComparator implements Comparator<Area> {
        @Override
        public int compare(Area o1, Area o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
}
