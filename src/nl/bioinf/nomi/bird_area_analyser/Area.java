/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.nomi.bird_area_analyser;

import java.util.Objects;

/**
 *
 * @author michiel
 */
class Area {
    private final String name;
    private final double area;
    private final int elevation;
    private final short soilTypesCount;
    private final double latitude;
    private final double distanceFromBritain;
    private int numberOfSpecies;

    /**
     *
     * @param name the name
     * @param area the area (square miles)
     * @param elevation the elevation (meters)
     * @param soilTypesCount the number of soil types
     * @param latitude the latitude (degrees)
     * @param distanceFromBritain distance from britina (miles)
     * @param numberOfSpecies  number of species observed
     */
    public Area(final String name,
            final double area,
            final int elevation,
            final short soilTypesCount,
            final double latitude,
            final double distanceFromBritain,
            final int numberOfSpecies) {
        this.name = name;
        this.area = area;
        this.elevation = elevation;
        this.soilTypesCount = soilTypesCount;
        this.latitude = latitude;
        this.distanceFromBritain = distanceFromBritain;
        this.numberOfSpecies = numberOfSpecies;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return area (square miles)
     */
    public double getArea() {
        return area;
    }

    /**
     *
     * @return elevation (meters)
     */
    public int getElevation() {
        return elevation;
    }

    /**
     *
     * @return soilTypes the number of soil types on the island
     */
    public short getSoilTypesCount() {
        return soilTypesCount;
    }

    /**
     *
     * @return latitude (degrees)
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     *
     * @return distance from Britain in miles.
     */
    public double getDistanceFromBritain() {
        return distanceFromBritain;
    }

    /**
     *
     * @return speciesCount the total number of species ever observed.
     */
    public int getNumberOfSpecies() {
        return numberOfSpecies;
    }

    /**
     *
     * @param numberOfSpecies the number of species to set.
     */
    public void setNumberOfSpecies(final int numberOfSpecies) {
        this.numberOfSpecies = numberOfSpecies;
    }

    @Override
    public String toString() {
        return "Area{" + "name=" + name + ", numberOfSpecies=" + numberOfSpecies + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Area other = (Area) obj;
        if (Double.doubleToLongBits(this.area) != Double.doubleToLongBits(other.area)) {
            return false;
        }
        if (this.elevation != other.elevation) {
            return false;
        }
        return Objects.equals(this.name, other.name);
    }
}
